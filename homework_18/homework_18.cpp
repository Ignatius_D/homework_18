﻿#include <iostream>
#include <string>

//Stack simulation
template<typename T>
class Stack {
private:
    int sizeArray = 0;
    T* stack = nullptr;
public:
    void push(T& element) {
        int newSizeArray = sizeArray + 1;
        T* newStack = new T[newSizeArray];
        newStack[0] = element;
        for (int i = 1; i < newSizeArray; i++) {
            newStack[i] = stack[i - 1];
        }
        delete[] stack;//Required
        stack = newStack;
        sizeArray = newSizeArray;
    }

    void pop() {
        if (sizeArray > 0) std::cout << "Head element: " << stack[0] << std::endl;
    }

    void printAllStack() {
        for (int i = 0; i < sizeArray; i++) {
            std::cout << stack[i] << std::endl;
        }
    }

    ~Stack() {
        delete[] stack;
        std::cout << "Stack is empty" << std::endl;
    }
};

template <typename T>
void commandsToStack(Stack<T>& stack) {
    bool isExit = false;
    while (!isExit) {
        std::string command;
        std::cout << "Choose command (push, pop, print, exit): " << std::endl;
        std::cin >> command;
        if (command == "push") {
            T element;
            std::cout << "Enter element: " << std::endl;
            std::cin >> element;
            stack.push(element);
        }
        else if (command == "pop") {
            stack.pop();
        }
        else if (command == "print") {
            stack.printAllStack();
        }
        else if (command == "exit") {
            isExit = true;
        }
        else {
            std::cerr << "Incorrect input!" << std::endl;
        }
    }
}

int main()
{
    bool isExit = false;
    while (!isExit) {
        std::string type;
        std::cout << "Choose type for stack (int, float, double, string): " << std::endl;
        std::cin >> type;
        isExit = true;
        if (type == "int") {
            Stack<int> stack;
            commandsToStack(stack);
        }
        else if (type == "float") {
            Stack<float> stack;
            commandsToStack(stack);
        }
        else if (type == "double") {
            Stack<double> stack;
            commandsToStack(stack);
        }
        else if (type == "string") {
            Stack<std::string> stack;
            commandsToStack(stack);
        }
        else {
            std::cerr << "Incorrect input!" << std::endl;
            isExit = false;
        }
    }
}

